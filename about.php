<!doctype html>
<html lang="en">
  <head>
  
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93035021-4"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-93035021-4');
</script>



    <link rel="apple-touch-icon-precomposed" href="img/favicon_152.png">

<!---IE 10 Metro tile icon (Metro equivalent of apple-touch-icon) ---->

    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="img/favicon_144.png">

<!--- Replace #FFFFFF with your desired tile color. ---->

<!--- IE 11 Tile for Windows 8.1 Start Screen ---->

    <meta name="application-name" content="Name">
    <meta name="msapplication-tooltip" content="Tooltip">
    <meta name="msapplication-config" content="xml/ieconfig.xml">

    
  

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Required meta tags -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
	
    <title>About - Daniel Waleczek</title>
  </head>
  <body>
  <div class="wrapper">

<?php include "content_managament/templates/include/header.php" ?>  

<main class="cointainer-fluid">

<?php
if($_GET['send']=='success'){
    echo '<div class="alert alert-success mx-4" role="alert">
        Mail send successfully!
    </div>';

}
else if($_GET['send']=='fail'){
    echo '<div class="alert alert-danger mx-4" role="alert">
        Failed to send mail!
    </div>';
}
?>




 
  <div class="container-fluid px-4 py-4">
		<div class="row">
			<div class="order-2 order-md-1 col-12 col-md-6 text-center ">
			<h1> The story </h1>
			
			
			<p class="text-justify textbloc">    Imagine yourself achieving your biggest goal. How does it feel? Would you settle down to enjoy your perfect being? Nah... it's not in human nature. I bet you would look for other things to experience, and it would be even more straightforward than before. 
				<br>
				<br>
						But to achieve the deepest dream, you have to put a lot of work into it. You need the proper mindset and the right people around you. If 4 people go hunting alone, they can catch rabbits, but a deer together. 
						<br>
						<br>
				I want to connect and help you and other people. Create a story about the success of many. Starting with videos that deliver different emotions. Then creating new journeys together, to finally establish a group of achievers. 

			</p>
			
			</div>
			<div class="order-1 order-md-2 col-12 col-md-6 py-4 text-center ">
			
			
				<picture>

					<source class="my-4 rounded-circle img" srcset="img/dw.webp" type="image/webp">
				  
				<source class="my-4 rounded-circle img" srcset="img/daniel_waleczek.jpg" type="image/jpeg"> 
				  
					<img class="my-4 rounded-circle img" src="img/daniel_waleczek.jpg" alt=""> 
				  
				  </picture>


			
			
			<br>
			
			
			<a href="https://www.facebook.com/IDanielWaleczek" target="_blank" class="mx-2"><i class="fab fa-facebook fa-2x text-dark"></i></a>
			<a href="https://www.instagram.com/idanielwaleczek/" target="_blank" class="mx-2"><i class="fab fa-instagram fa-2x text-dark"></i></a>
			<a href="mailto:contact@danielwaleczek.com" target="_blank" class="mx-2"><i class="far fa-envelope fa-2x text-dark"></i></a>
			
			</div>
			<div class="order-4 order-md-3  col-12 col-md-6 text-center mb-4 ">
			
			<h1> About me </h1>
			
				<p  class="text-justify textbloc"> Me? There is not much to say. Most people think I'm weird and crazy. I stopped to care, and instead, I put the energy on chasing dreams. My most important fantasy? To fly to the Moon. And then living is more peaceful when you have hopes that big.  </p>

			
			
			</div>
			<div class="order-3 order-md-4  col-12 col-md-6 text-center mb-4 ">
			
			<h1> Social proof </h1>
			
			<a href="https://www.youtube.com/watch?v=U6vYvJsACpo&fbclid=IwAR3CBnnre6JwriCJYgvb6SPMPsYZpZRl17UEQRRqXszgjFAabb4gHIFgsxI"  class="text-dark"><h2> Hitman Codename 47 Cinematic Movie</h2></a>
			
				<blockquote class="blockquote"><p  class="text-justify textbloc"> This video..... is unbelievable, I cannot believe there is a MovieGame cut of Codename 47 with dynamic free cam, intuitive dialogue editing and THE FUC**** CINEMATOGRAPHY. [...]
				<br>
				<br>
				Good on you sir, may this be a landmark for the Hitman fandom and still room for improvement for your skills as gameplay composer. Never take this video down! 
				<strong>for it is going to set a standard for years to come!!!!</strong> </p></blockquote>
				
					<a href="https://www.youtube.com/channel/UCRlABZBqja6i9Cj7fNC__Vw" class="text-dark"><strong> Thicc Boss 47</strong> </a>
					
					<br>
					
					<br>
					
					<br>
					
					<br>
					
					<br>
					
					<br>
					
				<p  class="text-justify textbloc">If you want to be listed here, leave some comments under my videos. Just be yourself and be honest. You can also send me an e-mail, but please remember that you will need to share a link to your website or YouTube channel.</p>
			
			</div>

			<!---------<div class="order-5 order-md-5  col-12 col-md-12 text-center my-4">
			
					<h1>Newsletter</h1>
					
					<p class="textbloc">
					As they say: If you want to change the world, start with yourself. <br>
					This is why I need your help. If you have any ideas, leave me a message. <br>
 

				    United we stand, divided we fall. <br>
					Join the story, sign up for the newsletter, improve your life, and help other people. 
					
					</p>
					<div class="row justify-content-center">
					<div class="col-12 col-sm-10 col-md-7 text-center ">
							<input class="form-control input-l" type="Email" name="EMAIL" placeholder="your@email.com"  />
							<input type="submit" class="btn btn-outline-dark input-r " name="saveChanges" value="Sign Up" />
					</div>
					</div>
					</div> -->


					<div class="order-5 order-md-5  col-12 col-md-12 justify-content-center text-center my-4">
					<a class="btn btn-outline-dark  my-4 mx-4" role="button" href="contact/index.php">Go to contact form!</a>
        	
					</div>

 
			</div>
 
		</div>
</main>
 
<?php include "content_managament/templates/include/footer.php" ?>
<!-- Footer -->
	</div>


	<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/about.css">
	<link rel="stylesheet" href="css/main.css">
	
	
	<script src="https://kit.fontawesome.com/2694440e40.js"></script>
	
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  
	
     <script src="bootstrap/dist/js/bootstrap.bundle.min.js" ></script>
    <script src="bootstrap/dist/js/bootstrap.min.js" ></script>
  </body>
</html>