<!doctype html>
<html lang="en">
  <head>
 

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93035021-4"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-93035021-4');
</script>

  <link rel="apple-touch-icon-precomposed" href="img/favicon_152.png">

<!---IE 10 Metro tile icon (Metro equivalent of apple-touch-icon) ---->

    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="img/favicon_144.png">

<!--- Replace #FFFFFF with your desired tile color. ---->

<!--- IE 11 Tile for Windows 8.1 Start Screen ---->

    <meta name="application-name" content="Name">
    <meta name="msapplication-tooltip" content="Tooltip">
    <meta name="msapplication-config" content="xml/ieconfig.xml">

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Required meta tags -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

   
	
    <title>Projects - Daniel Waleczek</title>
    
    	 <!-- Bootstrap CSS -->
	 <link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css">
	 <link rel="stylesheet" href="css/projects.css">
	 <link rel="stylesheet" href="css/main.css">
    
  </head>
  <body>
  <div class="wrapper">
  
  
   
<main class="cointainer-fluid">

 <div class="container-fluid px-4 py-4">
    
        	
			 
			
    
		<div class="row justify-content-center">
			<div class="col-12 col-md-5 py-4 px-4 mx-4 my-4  text-center jm-background rounded-lg">
			<a href="https://danielwaleczek.com/jm/about.php" class="no-format"><h1> Journey Motives </h1></a>
			
			 <a href="https://danielwaleczek.com/jm/about.php">
			
			<picture>

				<source class="my-4 rounded-circle img" srcset="img/projects/jm.webp" type="image/webp">
			  
				<source class="my-4 rounded-circle img" srcset="img/projects/jm.png" type="image/png"> 
			  
				<img class="my-4 rounded-circle img" src="img/projects/jm.png" alt=""> 
			  
			  </picture>
			
				</a>
			
				<p class="textbloc"> Life is a journey, and it's entirely up to you how it will end. On this channel, I'm going to travel and experience new things alone and with other people. It may be even you! Would you like to visit frozen lands, impassible rainforests, and islands with no people to admire them? 	</p>
			

			 <a href="https://www.youtube.com/channel/UCOEevxxzoleq0rgDgKE8azA" target="_blank" class="mx-2"><i class="fab fa-youtube fa-2x jm-background"></i></a>
			<a href="https://www.facebook.com/JourneyMotives/" target="_blank" class="mx-2"><i class="fab fa-facebook fa-2x jm-background"></i></a>
            
			<p>
				 <a class="btn btn-outline-danger  my-4 mx-4  d-md-none" role="button" href="https://danielwaleczek.com/jm/about.php">
			
	         Click to enter
			
				</a>
			
			</div>
			
	        
	        
	         
			<div class="col-12 col-md-5 py-4 px-4 mx-4 my-4 text-center gm-background rounded-lg">
			
			<a href="https://danielwaleczek.com/gm/about.php" class="no-format">
		<h1 class=" text-dark"> Game Motives </h1></a>
            
            <a href="https://danielwaleczek.com/gm/about.php">
            
			<picture>

				<source class="my-4 rounded-circle img" srcset="img/projects/gm.webp" type="image/webp">
			  
				<source class="my-4 rounded-circle img" srcset="img/projects/gm.png" type="image/png"> 
			  
				<img class="my-4 rounded-circle img" src="img/projects/gm.png" alt=""> 
			  
			  </picture>
            
            	</a>
			
			
			<p class=" text-dark textbloc"> Games are great stories. Why not use game engines to create nice-looking movies? It's only for entertainment, but wouldn't you want to see "games in real life" too? We could even record voices for our favorite characters that were never made. </p>

			<a href="https://www.youtube.com/user/eXe654/featured" target="_blank" class="mx-2"><i class="fab fa-youtube fa-2x text-dark"></i></a>
			<a href="https://www.facebook.com/Gamemotives/" target="_blank" class="mx-2"><i class="fab fa-facebook fa-2x  text-dark"></i></a>
            
            <p>
            
				 <a class="btn btn-outline-dark  my-4 mx-4  d-md-none" role="button" href="https://danielwaleczek.com/gm/about.php">
			
	         Click to enter
			
				</a>
			
			</div>
		
		    
			<!-------
			<div class="col-12 col-md-4 pb-4 text-center">
			
			
			<h1> Noise Motives </h1>
			
			<img class="my-4" src="img/projects/nm.jpg" alt="">
			
			<p> On this channel, you can find game soundtracks, great music playlists, and other audio-related stuff.
			</p>
				
				
			
			<a href="https://www.youtube.com/channel/UCWwBfFuZ4xJi6_smq9SlcvA/featured" target="_blank" class="mx-2"><i class="fab fa-youtube fa-2x text-dark"></i></a>
			<a href="" target="_blank" class="mx-2"><i class="fab fa-facebook fa-2x text-dark"></i></a>
            
			
			
			
			</div>
			
			---------->
			
		</div>

	</div>
				
	
	
 
 



</main>
 

<!-- Footer -->
	</div>


	 
	 
	 <script src="https://kit.fontawesome.com/2694440e40.js"></script>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  
	
     <script src="bootstrap/dist/js/bootstrap.bundle.min.js" ></script>
    <script src="bootstrap/dist/js/bootstrap.min.js" ></script>
  </body>
</html>