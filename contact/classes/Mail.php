<?php 



    require("/home/danielwa/config_mail.php");
	
		class Mail {
	
		    private $addressee = ADMIN_ADRESS;
		    private $headers;
		    public $subject;
		    public $content;
		    public $sender;
            
	

			public function send(){
	   
				        
				mail($this->addressee, $this->subject, $this->content, $this->headers);
				$copySubject = $this->subject . " (Copy returned to sender)";
				mail($this->sender, $copySubject, $this->content, $this->headers);
		
			}
	
			public function __construct($subject, $content, $sender){
	    
				$this->subject = $subject;
				$this->content = $content;
				$this->sender = $sender;     
				$this->headers = "From:" . $this->sender;
			}
		}

?>