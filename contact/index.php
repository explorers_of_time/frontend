<?php  
	
	require('classes/Mail.php');
	require('classes/CheckMail.php');
	
	
    if(isset($_POST['submit'])){	
            $sender = $_POST['email'];
            $subject = $_POST['subject'];
            $message = $_POST['message'];
   
	
	        $check = new CheckMail;

	        $isSecure = $check->sanitizeAdress($sender);
	        $content = $check->sanitizeString($message);

	        if ($isSecure == false) {
		        die(header('Location: https://danielwaleczek.com/about.php?send=fail', true, 302));
		        
            } else { //send email 
		    $mail = new Mail($subject, $content, $sender);
		    $mail->send();
		       die(header('Location: https://danielwaleczek.com/about.php?send=success', true, 302));
		       
	        }
        }
    ?>

<!DOCTYPE html>
<html>
<head>
    <title>Contact Form - Daniel Waleczek</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	
</head>
<body>
  <div class="wrapper">
     <?php include '../content_managament/templates/include/header.php'; ?>
    <div class="jumbotron">
        <div class="container">
            <h1>Do you need any help? Contact me.</h1>
            <p>I'll try to find a solution to your problem as soon as possible... and if not, well, you'll hear from me too.</p>
        </div>
    </div>
  

    <div class="col-md-6 col-md-offset-3">
        <form action="" id="contact-form" class="form-horizontal" method="post">
           
            <div class="form-group">
                <label for="form-email" class="col-lg-2 control-label">Your email: </label>
                <div class="col-lg-10">
                    <input type="email" class="form-control" id="form-email" name="email" placeholder="" required>
                </div>
            </div>
            <div class="form-group">
                <label for="form-subject" class="col-lg-2 control-label">Subject: </label>
                <div class="col-lg-10">
                    <input type="text" class="form-control" id="form-subject" name="subject" placeholder="" required>
                </div>
            </div>
            <div class="form-group">
                <label for="form-message" class="col-lg-2 control-label">Message:</label>
                <div class="col-lg-10">
                    <textarea class="form-control" rows="3" id="form-message" name="message" placeholder="" required></textarea>
                </div>
            </div>
            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                    <button type="submit" value="submit" name="submit" class="btn btn-outline-dark" >Send</button>
                </div>
            </div>
        </form>
    </div>
   
    </div>
    
    <!-- Bootstrap CSS -->
	 <link rel="stylesheet" href="../bootstrap/dist/css/bootstrap.min.css">
	 <link rel="stylesheet" href="../css/main.css">
	 
	 
	 <script src="https://kit.fontawesome.com/2694440e40.js"></script>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  
	
     <script src="../bootstrap/dist/js/bootstrap.bundle.min.js" ></script>
    <script src="../bootstrap/dist/js/bootstrap.min.js" ></script>
</body>
</html>