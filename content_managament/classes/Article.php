<?php



class Article
{


  /**
  * @var 
  */
  public $id = null;

  /**
  * @var 
  */
  public $dateOfPublication = null;

  /**
  * @var 
  */
  public $headline = null;

   public $WebP_img = null;

  public $img = null;

  /**
  * @var 
  */
  public $sneakPeak = null;

  /**
  * @var 
  */
  public $fullContent = null;

  public $pinned = null;
  
  public $category = null;
  /**
  * 
  *
  * @param 
  */

  public function __construct( $data=array() ) {
    if ( isset( $data['id'] ) ) $this->id = (int) $data['id'];
    if ( isset( $data['dateOfPublication'] ) ) $this->dateOfPublication = (int) $data['dateOfPublication'];
    if ( isset( $data['headline'] ) ) $this->headline = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9(){}[]]/", "", $data['headline'] );
    if ( isset( $data['WebP_img'] ) ) $this->WebP_img = $data['WebP_img'];
	if ( isset( $data['img'] ) ) $this->img = $data['img'];
    if ( isset( $data['sneakPeak'] ) ) $this->sneakPeak = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9(){}[]]/", "", $data['sneakPeak'] );
    if ( isset( $data['fullContent'] ) ) $this->fullContent = $data['fullContent'];
    if ( isset( $data['pinned'] ) ) $this->pinned = (int) $data['pinned'];
    if ( isset( $data['category'] ) ) $this->category = preg_replace ( "/[^\.\,\-\_\'\"\@\?\!\:\$ a-zA-Z0-9(){}[]]/", "", $data['category'] );
  }


  /**
  * 
  *
  * @param 
  */

  public function storeFormValues ( $params ) {

   
    $this->__construct( $params );

    
    if ( isset($params['dateOfPublication']) ) {
      $dateOfPublication = explode ( '-', $params['dateOfPublication'] );

      if ( count($dateOfPublication) == 3 ) {
        list ( $y, $m, $d ) = $dateOfPublication;
        $this->dateOfPublication = mktime ( 0, 0, 0, $m, $d, $y );
      }
    }
  }


  /**
  *
  *
  * @param 
  * @return 
  */

  public static function getById( $id ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT *, UNIX_TIMESTAMP(dateOfPublication)-TIMESTAMPDIFF(SECOND, NOW(), UTC_TIMESTAMP())   AS dateOfPublication FROM articles WHERE id = :id";
    $st = $conn->prepare( $sql );
    $st->bindValue( ":id", $id, PDO::PARAM_INT );
    $st->execute();
    $row = $st->fetch();
    $conn = null;
    if ( $row ) return new Article( $row );
  }


  /**
  * 
  *
  * @param 
  * @return 
  */

  public static function getList( $offset=0, $numRows=3 ) {
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "SELECT SQL_CALC_FOUND_ROWS *, UNIX_TIMESTAMP(dateOfPublication)-TIMESTAMPDIFF(SECOND, NOW(), UTC_TIMESTAMP())   AS dateOfPublication FROM articles
            ORDER BY pinned DESC, dateOfPublication DESC LIMIT $offset, :numRows" ;

    $st = $conn->prepare( $sql );
    $st->bindValue( ":numRows", $numRows, PDO::PARAM_INT );
    $st->execute();
    $list = array();

    while ( $row = $st->fetch() ) {
      $article = new Article( $row );
      $list[] = $article;
    }


    $sql = "SELECT FOUND_ROWS() AS totalRows";
    $totalRows = $conn->query( $sql )->fetch();
   
    $conn = null;
    return ( array ( "results" => $list, "totalRows" => $totalRows[0]) );
  }


  

  public function insert() {

    
    if ( !is_null( $this->id ) ) trigger_error ( "Article::insert(): Attempt to insert an Article object that already has its ID property set (to $this->id).", E_USER_ERROR );

    
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "INSERT INTO articles ( dateOfPublication, headline, WebP_img, img, sneakPeak, fullContent, pinned, category ) VALUES ( FROM_UNIXTIME(:dateOfPublication), :headline, :WebP_img, :img, :sneakPeak, :fullContent, :pinned, :category )";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":dateOfPublication", $this->dateOfPublication, PDO::PARAM_INT );
    $st->bindValue( ":headline", $this->headline, PDO::PARAM_STR );
	$st->bindValue( ":WebP_img", $this->WebP_img, PDO::PARAM_STR );
    $st->bindValue( ":img", $this->img, PDO::PARAM_STR );
    $st->bindValue( ":sneakPeak", $this->sneakPeak, PDO::PARAM_STR );
    $st->bindValue( ":fullContent", $this->fullContent, PDO::PARAM_STR );
    $st->bindValue( ":pinned", $this->pinned, PDO::PARAM_INT );
    $st->bindValue( ":category", $this->category, PDO::PARAM_STR );
    $st->execute();
    $this->id = $conn->lastInsertId();
    $conn = null;
  }




  public function update() {

    
    if ( is_null( $this->id ) ) trigger_error ( "Article::update(): Attempt to update an Article object that does not have its ID property set.", E_USER_ERROR );
   
   
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $sql = "UPDATE articles SET dateOfPublication=FROM_UNIXTIME(:dateOfPublication), headline=:headline, WebP_img=:WebP_img, img=:img, sneakPeak=:sneakPeak, fullContent=:fullContent, pinned=:pinned, category=:category WHERE id = :id";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":dateOfPublication", $this->dateOfPublication, PDO::PARAM_INT );
    $st->bindValue( ":headline", $this->headline, PDO::PARAM_STR );
	$st->bindValue( ":WebP_img", $this->WebP_img, PDO::PARAM_STR );
    $st->bindValue( ":img", $this->img, PDO::PARAM_STR );
    $st->bindValue( ":sneakPeak", $this->sneakPeak, PDO::PARAM_STR );
    $st->bindValue( ":fullContent", $this->fullContent, PDO::PARAM_STR );
    $st->bindValue( ":pinned", $this->pinned, PDO::PARAM_INT );
    $st->bindValue( ":category", $this->category, PDO::PARAM_STR );
    $st->bindValue( ":id", $this->id, PDO::PARAM_INT );
    $st->execute();
    $conn = null;
  }


 
  public function remove() {

   
    if ( is_null( $this->id ) ) trigger_error ( "Article::delete(): Attempt to delete an Article object that does not have its ID property set.", E_USER_ERROR );
	
	try {
		$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$st = $conn->prepare ( "DELETE FROM articles WHERE id = :id LIMIT 1" );
		$st->bindValue( ":id", $this->id, PDO::PARAM_INT );
		$st->execute();
		$conn = null;
	} catch (PDOException $e) {
		echo 'Connection failed: ' . $e->getMessage();
	}
  }

}

?>