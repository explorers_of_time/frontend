<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    

	
    <title>Hello, world!</title>
  </head>
  <body>
  <div class="wrapper">
  <?php include "templates/include/header_admin.php" ?>  
    <nav class="navbar navbar-expand-sm navbar-silver fixed-top navbar-light d-sm-none">
  <!-- Brand -->
  <a class="navbar-brand my-0 py-0" href="#">
	  
    <picture>
  
          <source class="logo my-0 py-0" srcset="../img/logo.webp" type="image/webp">
          
        <source class="logo my-0 py-0" srcset="../img/logo.png" type="image/png"> 
          
          <img class="logo my-0 py-0" src="../img/logo.png" alt=""> 
          
     </picture>
  
  
    </a>

  <!-- Toggler/collapsibe Button -->
  <button class="navbar-toggler no-outline" type="button" data-toggle="collapse" data-target="#collapsibleNavbar" >
		<i class="fas fa-ellipsis-h"></i>
  </button>
  
  <div class="collapse navbar-collapse footer-menu  px-4 py-4" id="collapsibleNavbar">
    <div class="container-fluid   ">
		<div class="row">
			<div class="order-1  col-12 col-sm-12 col-md-4  col-lg-4 col-xl-4 px-1 pt-3 text-center text-md-left">
				Copyright&nbsp;&copy; 2019, Daniel Walczek <br>
				<!----"Designed by" messege will go here--->
			</div>
			
			<div class="order-2  col-12 col-sm-12 col-md-8  col-lg-8 col-xl-8 px-1 pt-3 text-center	text-md-right ">
				<a href="https://www.facebook.com/JourneyMotives/" target="_blank" class="mx-2"><i class="fab fa-facebook fa-lg text-dark"></i></a>
              <!---<a href="https://twitter.com/IDanielWaleczek" target="_blank" class="mx-2"><i class="fab fa-twitter fa-3x text-dark"></i></a>-->
				<a href="https://www.youtube.com/channel/UCOEevxxzoleq0rgDgKE8azA" target="_blank" class="mx-2"><i class="fab fa-youtube fa-lg text-dark"></i></a>
				<a href="https://www.instagram.com/idanielwaleczek/" target="_blank" class="mx-2"><i class="fab fa-instagram fa-lg text-dark"></i></a>
                <a href="mailto:danielwaleczek@gmail.com" target="_blank" class="mx-2"><i class="far fa-envelope fa-lg text-dark"></i></a>
				
			</div>
			
			 
			
		</div>
		 </div>
	</div>
</nav>
 
 
 <nav class="navbar navbar-expand navbar-light navbar-silver bottom-menu fixed-bottom text-center py-0 px-0 d-sm-none">
 


  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link active" href="../news.html"><i class="fas fa-rss  fa-2x"></i><br><span class="d-inline-block mt-2">News</span></a>
    </li>
	
    <li class="nav-item">
      <a class="nav-link" href="../projects.html"><i class="fas fa-cogs  fa-2x"></i><br><span class="d-inline-block mt-2">Projects</span></a>
    </li>
	
	<li class="nav-item">
      <a class="nav-link" href="../about.html"><i class="fas fa-question-circle fa-2x"></i><br><span class="d-inline-block mt-2">About</span></a>
    </li>
    
	
  </ul>
 
 
 	
  
</nav>




 <nav class="navbar navbar-expand navbar-light navbar-silver main-menu fixed-top text-center py-0 px-0 d-none d-sm-flex">
 
	  <!-- Brand -->
    <a class="navbar-brand my-0 py-0" href="#">
	  
    <picture>
  
          <source class="logo my-0 py-0" srcset="../img/logo.webp" type="image/webp">
          
        <source class="logo my-0 py-0" srcset="../img/logo.png" type="image/png"> 
          
          <img class="logo my-0 py-0" src="../img/logo.png" alt=""> 
          
     </picture>
  
  
    </a>

  <ul class="navbar-nav">
    <li class="nav-item ">
      <a class="nav-link" href="admin.php?action=showArticles">Home</a>
    </li>
	
    <li class="nav-item active">
      <a class="nav-link" href="admin.php?action=addArticle">New article</a>
    </li>
	
	<li class="nav-item">
      <a class="nav-link" href="admin.php?action=signOut">Sign Out</a>
    </li>
    
	
  </ul>
 
 </nav>
</header>


<main class="container-fluid justify-content-center  px-4 py-4">
 <div id="adminHeader">
        <h2>Welcome</h2>
        <p>You are logged in as <b><?php echo htmlspecialchars( $_SESSION['username']) ?></b>. <a href="admin.php?action=signOut"?>Log out</a></p>
      </div>

      <h1><?php echo $results['pageheadline']?></h1>

      <form action="admin.php?action=<?php echo $results['formAction']?>" method="post">
        <input type="hidden" name="articleId" value="<?php echo $results['article']->id ?>"/>

<?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
<?php } ?>

 

     
            <label for="headline">Article title</label><br>
            <input class="form-control" type="text" name="headline" id="headline" placeholder="Name of the article" required autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['article']->headline )?>" />
          	<br>
			
			 <label for="img">WebP Image file</label><br>
            <input class="form-control" type="text" name="WebP_img" id="WebP_img" placeholder="Name and extension of WebP image from img directory " autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['article']->WebP_img )?>" />
          	<br>
			
            <label for="img">Image file</label><br>
            <input class="form-control" type="text" name="img" id="img" placeholder="Name and extension of image from img directory " autofocus maxlength="255" value="<?php echo htmlspecialchars( $results['article']->img )?>" />
          	<br>
          	
          	
            <label for="sneakPeak">Article summary</label><br>
            <textarea class="form-control" name="sneakPeak" id="sneakPeak" placeholder="Brief description of the article" required maxlength="1000" ><?php echo htmlspecialchars( $results['article']->sneakPeak )?></textarea>
          	<br>
          	
            <label for="fullContent">Full article</label><br>
            <textarea class="form-control" name="fullContent" id="fullContent" placeholder="The HTML code of the article" required maxlength="100000" ><?php echo htmlspecialchars( $results['article']->fullContent )?></textarea>
            <br>

          
            <label for="dateOfPublication">Publication Date</label><br>
            <input class="form-control" type="date" name="dateOfPublication" id="dateOfPublication" placeholder="YYYY-MM-DD" required maxlength="10" value="<?php echo $results['article']->dateOfPublication ? date( "Y-m-d", $results['article']->dateOfPublication ) : "" ?>" />
          	<br>
          	
          	<label for="category">Category</label><br>
            <textarea class="form-control" name="category" id="category" placeholder="Article category" maxlength="255" ><?php echo htmlspecialchars( $results['article']->category )?></textarea>
          	<br>

            <div class="form-check">
				  <input type="hidden" name="pinned" value="0" />
                  <input type="checkbox" class="form-check-input" name="pinned" id="pinned" maxlength="5" value="1" <?php if ($results['article']->pinned == 1) echo "checked='checked'"; ?> > 
            
                  <label class="form-check-label" for="pinned" >Pin the article</label>
            </div>
            
     

  
        <div class="buttons">
          <input type="submit" class="btn btn-outline-dark   my-4 mx-4" name="saveChanges" value="Save Changes" /></button>
          <input type="submit" class="btn btn-outline-dark   my-4 mx-4" formnovalidate name="cancel" value="Cancel" /></button>
        </div>

      </form>

     

<?php if ( $results['article']->id ) { ?>
      <p><a href="admin.php?action=removeArticle&amp;articleId=<?php echo $results['article']->id ?>" onclick="return confirm('Delete This Article?')">Delete This Article</a></p>
<?php } ?>

</main>
 
<?php include "templates/include/footer.php" ?>
<!-- Footer -->
  </div>
  
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="../bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/news.css">
	<link rel="stylesheet" href="../css/main.css">
	
	
	<script src="https://kit.fontawesome.com/2694440e40.js"></script>
	

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  
	
     <script src="../bootstrap/dist/js/bootstrap.bundle.min.js" ></script>
    <script src="../bootstrap/dist/js/bootstrap.min.js" ></script>
  </body>
</html>