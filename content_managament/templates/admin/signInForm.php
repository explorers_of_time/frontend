<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

   
  
    <title>Hello, world!</title>
  </head>
  <body>
  <div class="wrapper">
  <?php include "templates/include/header.php" ?>  
 


<main class="container-fluid justify-content-center text-center px-4 py-4">
       <form action="admin.php?action=signIn" method="post" >
        <input type="hidden" name="signIn" value="true" />

<?php if ( isset( $results['errorMessage'] ) ) { ?>
        <div class="errorMessage"><?php echo $results['errorMessage'] ?></div>
<?php } ?>

        
            <label for="username">Username</label><br>
            <input class="form-control" type="text" name="username" id="username" placeholder="Your admin username" required autofocus maxlength="20" />
            <br>
            <br>
            <label for="password">Password</label><br>
            <input class="form-control" type="password" name="password" id="password" placeholder="Your admin password" required maxlength="20" />

         <br> 

        <div class="buttons">
          <input type="submit" name="signIn" value="Login" class="btn btn-outline-dark   my-4 mx-4" />
        </div>

      </form>
      </form>


</main>

<?php include "templates/include/footer.php" ?>
<!-- Footer -->
  </div>
   <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="../bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/news.css">
	<link rel="stylesheet" href="../css/main.css">
	
	
	<script src="https://kit.fontawesome.com/2694440e40.js"></script>
	

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  
	
     <script src="../bootstrap/dist/js/bootstrap.bundle.min.js" ></script>
    <script src="../bootstrap/dist/js/bootstrap.min.js" ></script>
  </body>
</html>