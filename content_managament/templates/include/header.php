<?php 
	$current_page = basename($_SERVER['PHP_SELF']);
?>

<header>
    <nav class="navbar navbar-expand-sm navbar-silver fixed-top navbar-light d-sm-none">
  <!-- Brand -->
  <a class="navbar-brand my-0 py-0" href="https://danielwaleczek.com/">
	  
		<picture>
	
					<source class="logo my-0 py-0" srcset="https://danielwaleczek.com/img/logo.webp" type="image/webp">
				  
				<source class="logo my-0 py-0" srcset="https://danielwaleczek.com/img/logo.png" type="image/png"> 
				  
					<img class="logo my-0 py-0" src="https://danielwaleczek.com/img/logo.png" alt=""> 
				  
		 </picture>
	
	
		</a>

  <!-- Toggler/collapsibe Button -->
  <button class="navbar-toggler no-outline" type="button" data-toggle="collapse" data-target="#collapsibleNavbar" >
		<i class="fas fa-ellipsis-h"></i>
  </button>
  
  <div class="collapse navbar-collapse footer-menu  px-4 py-4" id="collapsibleNavbar">
    <div class="container-fluid   ">
		<div class="row">
			<div class="order-1  col-12 col-sm-12 col-md-4  col-lg-4 col-xl-4 px-1 pt-3 text-center text-md-left">
				Copyright&nbsp;&copy; 2019, Daniel Waleczek <br>
				Designed by Krystian Stebel
				<!----"Designed by" messege will go here--->
			</div>
			
			<div class="order-2  col-12 col-sm-12 col-md-8  col-lg-8 col-xl-8 px-1 pt-3 text-center	text-md-right ">
				<a href="https://www.facebook.com/JourneyMotives/" target="_blank" class="mx-2"><i class="fab fa-facebook fa-lg text-dark"></i></a>
              <!---<a href="https://twitter.com/IDanielWaleczek" target="_blank" class="mx-2"><i class="fab fa-twitter fa-3x text-dark"></i></a>-->
				<a href="https://www.youtube.com/channel/UCOEevxxzoleq0rgDgKE8azA" target="_blank" class="mx-2"><i class="fab fa-youtube fa-lg text-dark"></i></a>
				<a href="https://www.instagram.com/idanielwaleczek/" target="_blank" class="mx-2"><i class="fab fa-instagram fa-lg text-dark"></i></a>
                <a href="mailto:contact@danielwaleczek.com" target="_blank" class="mx-2"><i class="far fa-envelope fa-lg text-dark"></i></a>
				
			</div>
			
			 
			
		</div>
		 </div>
	</div>
</nav>
 <nav class="navbar navbar-expand navbar-light navbar-silver bottom-menu fixed-bottom text-center py-0 px-0 d-sm-none">
 


  <ul class="navbar-nav">
    <li class="nav-item  <?php echo ($current_page == "news.php" ? "active" : "")?>">
      <a class="nav-link  " href="https://danielwaleczek.com/content_managament/news.php"><i class="fas fa-rss  fa-2x"></i><br><span class="d-inline-block mt-2">
	   News</span></a>
    </li>
	
    <li class="nav-item  <?php echo ($current_page == "projects.php" ? "active" : "")?> ">
      <a class="nav-link" href="https://danielwaleczek.com/projects.php"><i class="fas fa-cogs  fa-2x"></i><br><span class="d-inline-block mt-2">Projects</span></a>
    </li>
	
	<li class="nav-item <?php echo ($current_page == "about.php" ? "active" : "")?>">
      <a class="nav-link " href="https://danielwaleczek.com/about.php"><i class="fas fa-question-circle fa-2x"></i><br><span class="d-inline-block mt-2">About</span></a>
    </li>
    
	
  </ul>
 
 
 	
  
</nav>




 <nav class="navbar navbar-expand navbar-light navbar-silver main-menu fixed-top text-center py-0 px-0 d-none d-sm-flex">
 
	  <!-- Brand -->
  <a class="navbar-brand my-0 py-0" href="https://danielwaleczek.com/">
	  
	<picture>

				<source class="logo my-0 py-0" srcset="https://danielwaleczek.com/img/logo.webp" type="image/webp">
			  
			<source class="logo my-0 py-0" srcset="https://danielwaleczek.com/img/logo.png" type="image/png"> 
			  
				<img class="logo my-0 py-0" src="https://danielwaleczek.com/img/logo.png" alt=""> 
			  
	 </picture>


	</a>

  <ul class="navbar-nav">
    <li class="nav-item <?php echo ($current_page == "news.php" ? "active" : "")?> ">
      <a class="nav-link" href="https://danielwaleczek.com/content_managament/news.php">News</a>
    </li>
	
    <li class="nav-item <?php echo ($current_page == "projects.php" ? "active" : "")?> ">
      <a class="nav-link" href="https://danielwaleczek.com/projects.php">Projects</a>
    </li>
	
	<li class="nav-item <?php echo ($current_page == "about.php" ? "active" : "")?> ">
      <a class="nav-link " href="https://danielwaleczek.com/about.php">About</a>
    </li>
    
	
  </ul>
 
 </nav>
</header>
