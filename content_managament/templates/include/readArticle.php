<!doctype html>
<html lang="en">
  <head>
  
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93035021-4"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-93035021-4');
</script>


  <link rel="apple-touch-icon-precomposed" href="https://danielwaleczek.com/img/favicon_152.png">

<!---IE 10 Metro tile icon (Metro equivalent of apple-touch-icon) ---->

    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="https://danielwaleczek.com/img/favicon_144.png">

<!--- Replace #FFFFFF with your desired tile color. ---->

<!--- IE 11 Tile for Windows 8.1 Start Screen ---->

    <meta name="application-name" content="Name">
    <meta name="msapplication-tooltip" content="Tooltip">
    <meta name="msapplication-config" content="https://danielwaleczek.com/xml/ieconfig.xml">

  
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Required meta tags -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
 
	
    <title><?php echo htmlspecialchars( $results['article']->headline )?> - Daniel Waleczek</title>
  </head>
  <body>
  <div class="wrapper">
 
	<?php include "header.php" ?>  

<main class="container-fluid content flex-column justify-content-center text-justify px-4 py-4">
	  
	   <h1 class="headline" ><?php echo htmlspecialchars( $results['article']->headline )?></h1>
	   
     <div class="intro" ><!--<img src="../img/<?/*php echo $results['article']->img */?> " alt=""  class="py-4 artimage" style="height: auto;">-->
	  <?php echo htmlspecialchars( $results['article']->sneakPeak )?></div><br>
      <div class="fullContent" ><?php echo $results['article']->fullContent?></div><br>
      <p class="pubDate">Published on <?php echo date('j F Y', $results['article']->dateOfPublication)?></p>

      

</main>
 
<?php include "footer.php" ?>
<!-- Footer -->
	</div>

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="../bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/news.css">
	<link rel="stylesheet" href="../css/main.css">
	<link rel="stylesheet" href="../css/readArticle.css">
	
	<script src="https://kit.fontawesome.com/2694440e40.js"></script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  
	
     <script src="../bootstrap/dist/js/bootstrap.bundle.min.js" ></script>
    <script src="../bootstrap/dist/js/bootstrap.min.js" ></script>
  </body>
</html>