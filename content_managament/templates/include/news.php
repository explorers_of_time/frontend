<!doctype html>
<html lang="en">
  <head>
  
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-93035021-4"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-93035021-4');
</script>

  <link rel="apple-touch-icon-precomposed" href="https://danielwaleczek.com/img/favicon_152.png">

<!---IE 10 Metro tile icon (Metro equivalent of apple-touch-icon) ---->

    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="https://danielwaleczek.com/img/favicon_144.png">

<!--- Replace #FFFFFF with your desired tile color. ---->

<!--- IE 11 Tile for Windows 8.1 Start Screen ---->

    <meta name="application-name" content="Name">
    <meta name="msapplication-tooltip" content="Tooltip">
    <meta name="msapplication-config" content="https://danielwaleczek.com/xml/ieconfig.xml">

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- Required meta tags -->
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
	
    <title>News - Daniel Waleczek</title>
  </head>
  <body>
  <div class="wrapper">
 
 <?php include "header.php" ?>  

 <main class="cointainer-fluid">

<div class="container-fluid px-4 py-4">
		<div class="row">
			
   
				
      <?php foreach ( $results['articles'] as $article) { ?>
        
     
            <?php if($article->category==$_GET['category'] || $_GET['category']=='') : ?>
   
        

        	<div class="col-12 col-md-4 text-justify">
				<div class="row text-center justify-content-center px-1"">
			
        		<div class="col-12 py-4 my-4 text-justify rounded-lg <?php if ($article->pinned == 1) echo "pinned"; ?> ">
        		
            <h3> <?php echo htmlspecialchars( $article->headline )?>  </h3>
            
            <span class="pubDate py-4"><?php echo date('j F Y', $article->dateOfPublication)?></span>

           

            <?php if($article->img!="" or $article->WebP_img!="") : ?>
                <br>
				<a  href="news.php?action=readArticle&amp;articleId=<?php echo $article->id?>">
                <picture>

				<source class="py-4" srcset="../img/<?php echo $article->WebP_img ?> " type="image/webp" style="width: 100%; height: auto;">
			  
				<source class="py-4" srcset="../img/<?php echo $article->img ?> " type="image/png" style="width: 100%; height: auto;"> 
			  
				<img class="py-4" src="../img/<?php echo $article->img ?> " alt="" style="width: 100%; height: auto;"> 
			  
			  </picture>
             
				
			   </a>
               
            
				 <br>

            
            <?php endif; ?>
         
       
          
          <p class="sneakPeak py-4"><?php echo htmlspecialchars( $article->sneakPeak )?></p>
        	
         
     <a class="btn btn-outline-dark float-right my-4 mx-4" role="button" href="news.php?action=readArticle&amp;articleId=<?php echo $article->id?>">Read more</a>
				
				
				</div>
				</div>
			</div>

      
 
          <?php endif; ?>

			<?php } ?> 
				
			
			
			
			
			
			
			
		</div>

	</div>


  <ul class="pagination justify-content-center">
		<?php if($page!=1) : ?>
			<li class="page-item"><a class="page-link" href="?id=1">1</a></li>
		<?php endif; ?> 
		<?php if($page-3 > 1) : ?>
		 <li class="page-item">
            <a class="page-link" href="<?php if($page <= 1){ echo '#'; } else { echo "?id=".($page - 3); } ?>">-3</a>
        </li>
		<?php endif; ?> 
		<?php if($page-1 > 1) : ?>
		 <li class="page-item">
            <a class="page-link" href="<?php if($page <= 1){ echo '#'; } else { echo "?id=".($page - 1); } ?>"><?php echo $page - 1;  ?></a>
        </li>
       <?php endif; ?> 
		 <li class="page-item active">
            <a class="page-link" href="<?php if($page <= 1){ echo '#'; } else { echo "?id=".($page); } ?>"><?php echo $page;  ?></a>
        </li>
		<?php if($page+1 < $results['totalPages']) : ?>
        <li class="page-item ">
            <a class="page-link" href="<?php if($page >= $results['totalPages']){ echo '#'; } else { echo "?id=".($page + 1); } ?>"><?php echo $page + 1;  ?></a>
        </li>
		<?php endif; ?> 
		<?php if($page + 3 < $results['totalPages']) : ?>
		<li class="page-item ">
            <a class="page-link" href="<?php if($page >= $results['totalPages']){ echo '#'; } else { echo "?id=".($page + 3); } ?>">+3</a>
        </li>
		<?php endif; ?> 
		<?php if($page!=$results['totalPages']) : ?>
        <li class="page-item"><a class="page-link" href="?id=<?php echo $results['totalPages']; ?>"><?php echo $results['totalPages'];  ?></a></li>
		<?php endif; ?> 
    </ul>



 
   	      



      
 
 
 
 
 </main>
 

<?php include "footer.php" ?>

<!-- Footer -->
  </div>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="../bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/news.css">
	<link rel="stylesheet" href="../css/main.css">
	
	
	<script src="https://kit.fontawesome.com/2694440e40.js"></script>
	

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
	
		 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  
	
     <script src="../bootstrap/dist/js/bootstrap.bundle.min.js" ></script>
    <script src="../bootstrap/dist/js/bootstrap.min.js" ></script>
  </body>
</html>