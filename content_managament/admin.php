<?php

require( "/home/danielwa/config.php" );

session_start();
$action = isset( $_GET['action'] ) ? $_GET['action'] : "";
$username = isset( $_SESSION['username'] ) ? $_SESSION['username'] : "";


if ( $action != "signIn" && $action != "signOut" && !$username ) {
  signIn();
  exit;
}

switch ( $action ) {
  case 'signIn':
    signIn();
    break;
  case 'signOut':
    signOut();
    break;
  case 'addArticle':
    addArticle();
    break;
  case 'modifyArticle':
    modifyArticle();
    break;
  case 'removeArticle':
    removeArticle();
    break;
  default:
    showArticles();
}


function signIn() {
  $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD ); // set up PDO with values defined in config.php
  $results = array();
  $results['pageheadline'] = "Login to admin panel";
  $salt = "placeholder"; // use the same value as in doNotSendToServer.php

  if ( isset( $_POST['signIn'] ) ) {

  
    
     // get values from log in form
    $username = !empty($_POST['username']) ? trim($_POST['username']) : null;
    $passwordToCheck = !empty($_POST['password']) ? trim($_POST['password']) : null;
   
	
	
     // find username in database and get other values for that row
    $query = "SELECT id, name, password, isAdmin FROM users WHERE name = :username";
    $statement = $conn->prepare($query);
    
    
    $statement->bindValue(':username', $username);
    
   
    $statement->execute();
    
     // if username exist in the database fetch values from row and save them to $user array
    $user = $statement->fetch(PDO::FETCH_ASSOC);
    
    // if username is absent from the database
    if($user === false){
        die('Wrong password or username!');
    } else{
       
		
        
        //check if password matches the one in database
        if(password_verify($passwordToCheck, $user['password'])){
            
           
            
            
			if($user['isAdmin']){
				 //start admin session
				$_SESSION['user_id'] = $user['id'];
				$_SESSION['logged_in'] = time();
				$_SESSION['username'] = $user['name'];
				header( "Location: admin.php" );
			} else {
				// placeholder for normal user session and panel
				 die('You do not have admin priviliges!');
			}
			
			
            
            exit;
            
        } else{ // password from the form was invalid
            
		
            die('Wrong password or username!');
        }
   
	}
  } else {

    require( TEMPLATE_PATH . "/admin/signInForm.php" );
  }

}


function signOut() {
  unset( $_SESSION['username'] );
  header( "Location: admin.php" );
}


function addArticle() {

  $results = array();
  $results['pageheadline'] = "New Article";
  $results['formAction'] = "addArticle";

  if ( isset( $_POST['saveChanges'] ) ) {

   
    $article = new Article;
    $article->storeFormValues( $_POST );
    $article->insert();
    header( "Location: admin.php?status=changesSaved" );

  } elseif ( isset( $_POST['cancel'] ) ) {

    
    header( "Location: admin.php" );
  } else {

   
    $results['article'] = new Article;
    require( TEMPLATE_PATH . "/admin/modifyArticle.php" );
  }

}


function modifyArticle() {

  $results = array();
  $results['pageheadline'] = "Edit Article";
  $results['formAction'] = "modifyArticle";

  if ( isset( $_POST['saveChanges'] ) ) {

    

    if ( !$article = Article::getById( (int)$_POST['articleId'] ) ) {
      header( "Location: admin.php?error=articleNotFound" );
      return;
    }

    $article->storeFormValues( $_POST );


    $article->update();
    header( "Location: admin.php?status=changesSaved" );

  } elseif ( isset( $_POST['cancel'] ) ) {

    
    header( "Location: admin.php" );
  } else {

    
    $results['article'] = Article::getById( (int)$_GET['articleId'] );
    require( TEMPLATE_PATH . "/admin/modifyArticle.php" );
  }

}


function removeArticle() {

  if ( !$article = Article::getById( (int)$_GET['articleId'] ) ) {
    header( "Location: admin.php?error=articleNotFound" );
    return;
  }

  $article->remove();
  header( "Location: admin.php?status=articleDeleted" );
}


function showArticles() {
  $results = array();
  $data = Article::getList(0,999999999999);
  $results['articles'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $results['pageheadline'] = "All Articles";

  if ( isset( $_GET['error'] ) ) {
    if ( $_GET['error'] == "articleNotFound" ) $results['errorMessage'] = "Error: Article not found.";
  }

  if ( isset( $_GET['status'] ) ) {
    if ( $_GET['status'] == "changesSaved" ) $results['statusMessage'] = "Your changes have been saved.";
    if ( $_GET['status'] == "articleDeleted" ) $results['statusMessage'] = "Article deleted.";
  }

  require( TEMPLATE_PATH . "/admin/showArticles.php" );
}

?>