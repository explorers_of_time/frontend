<?php

require( "/home/danielwa/config.php" );
$action = isset( $_GET['action'] ) ? $_GET['action'] : "";

switch ( $action ) {
  case 'newsList':
    newsList();
    break;
  case 'readArticle':
    readArticle();
    break;
  default:
    mainPage();
}


function newsList() {
  $results = array();
  $data = Article::getList();
  $results['articles'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];
  $results['pageheadline'] = "Article Archive | Widget News";
  require( TEMPLATE_PATH . "/include/news.php" );
}

function readArticle() {
  if ( !isset($_GET["articleId"]) || !$_GET["articleId"] ) {
    mainPage();
    return;
  }

  $results = array();
  $results['article'] = Article::getById( (int)$_GET["articleId"] );
  $results['pageheadline'] = $results['article']->headline . " | Widget News";
  require( TEMPLATE_PATH . "/include/readArticle.php" );
}

function mainPage() {
  if (isset($_GET['id'])) {
    $page = $_GET['id'];
  } else {
    $page = 1;
  }
  $results = array();
  $numRows = 6;
  $totalPages;
  $offset = ($page-1) * $numRows;
 // while(Article::getList != null){
  $data = Article::getList($offset, $numRows);
  $results['articles'] = $data['results'];
  $results['totalRows'] = $data['totalRows'];

  $results['totalPages'] = ceil($results['totalRows'] / $numRows);
  $results['pageheadline'] = "Widget News";
  require( TEMPLATE_PATH . "/include/news.php" );
}

?>