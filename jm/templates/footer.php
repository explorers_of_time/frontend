
	<!-- Footer -->
<footer class="page-footer font-small jm py-1 px-4 sticky-bottom d-none d-sm-block">
	<div class="container-fluid">
		<div class="row">
			<div class="order-2 order-md-1 col-12 col-sm-12 col-md-4  col-lg-4 col-xl-4 px-1 pt-4 text-center text-danger text-md-left">
				
				Copyright&nbsp;&copy; 2019, Daniel Waleczek <br>
				Designed by Krystian Stebel
				<!----"Designed by" messege will go here--->
				
			</div>
			
			
			<div class="order-1 order-md-2 col-12 col-sm-12 col-md-6  col-lg-6 col-xl-6 px-1 pt-4 text-center	text-md-left ">
				<a href="https://www.facebook.com/IDanielWaleczek" target="_blank" class="mx-2"><i class="fab fa-facebook fa-3x  text-danger"></i></a>
              <!---<a href="https://twitter.com/IDanielWaleczek" target="_blank" class="mx-2"><i class="fab fa-twitter fa-3x  text-danger"></i></a>-->
				<a href="https://www.youtube.com/channel/UCOEevxxzoleq0rgDgKE8azA" target="_blank" class="mx-2"><i class="fab fa-youtube fa-3x  text-danger"></i></a>
				<a href="https://www.instagram.com/idanielwaleczek/" target="_blank" class="mx-2"><i class="fab fa-instagram fa-3x  text-danger"></i></a>
                <a href="mailto:contact@danielwaleczek.com" target="_blank" class="mx-2"><i class="far fa-envelope fa-3x  text-danger"></i></a>
				
			</div>
			
				<div class="order-3 order-md-3 col-12 col-sm-12 col-md-1  col-lg-1 col-xl-1 px-1 pt-1 text-center text-md-right">
				
				<!-- GreenGeeks Seal Code -->
				<div><a href="#" onclick="ggs_ggseal()"><img src="https://static.websitehostserver.net/ggseal/Green_19.png"></a><script>function ggs_ggseal(){window.open("https://my.greengeeks.com/seal/","_blank")}</script></div>
				<!-- End GreenGeeks Seal Code -->
				
				
				
			</div>
			
			
			
			
			
		</div>
	</div>
	


</footer>